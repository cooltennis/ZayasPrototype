--Define vars
math.randomseed(os.time())
level = 1
gold = 0
monsterHealth = 0
room = {}
roomExit = {}
Item = {}
inventory = {}
playerPos = {math.random(0,3),math.random(0,3)} --starts player in random position if it doesn't work fucking end me

function love.load()
	roomCard = love.graphics.newImage("roomcard.png")
	adjRoomCard =love.graphics.newImage("adjacent_roomcard.png")
	playerIcon = love.graphics.newImage("player.png")
	lootDeck = love.graphics.newImage("loot_button.png")
	exitRoom = love.graphics.newImage("found_exit.png")
	ghost = love.graphics.newImage("spooper.png")
	attackButton = love.graphics.newImage("attack_button.png")
end

function addMonster()
	x = math.random(0,3)
	y = math.random(0,3)
	if x == roomExit[1] and y == roomExit[2] then
		addMonster()
	end
	room[x][y][2] = room[x][y][2]+1
end

function createRoom()
	foundExit = false;
	roomExit = {math.random(0,3),math.random(0,3)}	
	for x=0, 3 do
		room[x] = {}
		for y=0,3 do
			room[x][y] = {math.random(1,9), 0} -- represents room[x][y]: (lootchance as percentage/10, number of monsters)
		--room[x][y].lootChance = {math.random(1,9)}
		--room[x][y].monsters = {math.random(0,level)}
		end
	end
	for i = 0, (2 + level*2) do
		addMonster()
	end
	print("room exit: " .. roomExit[1] .. "," .. roomExit[2])
end

function removeMonster(x,y)
	room[x][y][2] = room[x][y][2] - 1
	print(room[x][y][2])	
	print(x..y)
end

function Item:new (o,itemType,itemTier)
	o = o or {}
	setmetatable(o, self)
	self.__index = self
	self.itemType = itemType or "health"
	self.itemStrength = itemStrength or 1
	return o
end

function drawLoot(x,y)
	if (math.random(0,100) <= room[x][y][1]) then
		if (inventory[item] ~= nil) then
			inventory[item] = inventory[item] + amount
		else
			inventory[item] = amount
		end
	else
		gold = gold + math.random(level,level*5)
	end
end

function checkMonster(x,y)
	if room[x][y][2] <= 0 then
		return false
	else
		return true
	end
end

function movePlayer(x,y)
	print(checkMonster(x,y))
	playerPos = {x,y}
	--addMonster()
	if playerPos[1] == roomExit[1] and playerPos[2] == roomExit[2] then
		foundExit = true
		print("Found Exit!")
	else
		if checkMonster(x,y) then
			monsterHealth = (math.random(level*100,(level+1)*100))
		end
		foundExit = false
	end
end


function checkAdjacency(x,y) --check if adjacent to the player's current position, not including diagonally.
	if ((y-1 == playerPos[2]) or (y+1 == playerPos[2])) and (x == playerPos[1])
	or ((x-1 == playerPos[1]) or (x+1 == playerPos[1])) and (y == playerPos[2]) then
	return true
	end
	return false
end

function love.mousepressed(mx, my, button)
	cardPosX = 0
	cardPosY = 0
		if mx > 50 and mx < 610 and my > 50 and my < 610 then
			if (mx-50)/140 > 3 then
				cardPosX = 3
			elseif (mx-50)/140 > 2 then
				cardPosX = 2
			elseif (mx-50)/140 > 1 then
				cardPosX = 1
			else
				cardPosX = 0
			end
				
			if (my-50)/140 > 3 then
				cardPosY = 3
			elseif (my-50)/140 > 2 then
				cardPosY = 2
			elseif (my-50)/140 > 1 then
				cardPosY = 1
			else
				cardPosY = 0
			end		
			if checkAdjacency(cardPosX,cardPosY) then
				movePlayer(cardPosX,cardPosY)
			end
		end	
	print(cardPosX .. cardPosY)
	if button == 1 then
		if mx >= 630 and mx < 630 + lootDeck:getWidth() and my >= 20  and my < 20  + lootDeck:getHeight() then
			print("drawing loot!")
		elseif foundExit and mx >= 630 and mx < 630 + exitRoom:getWidth() and my >= 100  and my < 100  + exitRoom:getHeight() then
			level = level + 1
			createRoom()
			print("Creating new floor")
		elseif checkMonster(playerPos[1],playerPos[2]) and mx >= 630 and mx < 630 + attackButton:getWidth() and my >= 100  and my < 100  + attackButton:getHeight() then
			--print("moved and monster in place")
			if monsterHealth > 0 then
				monsterHealth = monsterHealth - 50
				if monsterHealth <= 0 then
					--print("ducking zaya u cuck")
					removeMonster(playerPos[1],playerPos[2]) --code was calling with cardPosY and cardPosY, these where set to be 00 because thats the button that was clicked
					--print(checkMonster(cardPosX, cardPosY))
				end
			end
		end
	end
end

function love.draw()
    love.graphics.print("zdawg's shitty prototype", 30, 8)
	love.graphics.print("Level " .. level .. "  Find the exit!", 30, 25)	
	love.graphics.draw(lootDeck,630,20)
	if foundExit then
		love.graphics.draw(exitRoom,630,100)
	end
	for x=0, 3 do
		for y=0,3 do
			if checkAdjacency(x,y) then
			love.graphics.draw(adjRoomCard, x*140+50, y*140+50)
			else
			love.graphics.draw(roomCard, x*140+50, y*140+50)
			end			
		end
	end
	love.graphics.draw(playerIcon, playerPos[1]*140+50, playerPos[2]*140+50)
	if checkMonster(playerPos[1],playerPos[2]) and monsterHealth > 0 then
		love.graphics.draw(ghost,playerPos[1]*140+50, playerPos[2]*140+50)
		love.graphics.print("Monster Health: " .. monsterHealth ,20,660)
		love.graphics.draw(attackButton,630,100)
	end
end

function love.update(dt)
end

--Main program
createRoom()

